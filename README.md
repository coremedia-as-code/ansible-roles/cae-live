# CoreMedia - cae-live

Supports multiple installation of CAEs.

define CAE instance id in the playbook

```
- role: cae-live
  vars:
    - instances: 3
```


## Ports

```
42${instance_id}05
42${instance_id}99
42${instance_id}98
42${instance_id}80
42${instance_id}09
```

## dependent services

### replication-live-server
```
cae_live:
  repository:
    url: http://replication-live-server.int:42080/replication-live-server/ior
```

### solr
```
cae_live:
  solr:
    url: http://127.0.0.1:40080/solr
```
### elastic
```
cae_live:
  elastic:
    solr:
      url: http://127.0.0.1:40080/solr
```
