import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/cae-live-1/server-lib",
    "/opt/coremedia/cae-live-1/common-lib",
    "/opt/coremedia/cae-live-1/current/webapps/cae-live-1/WEB-INF/properties/corem",
    "/var/cache/coremedia/cae-live-2/blobstore-assets",
    "/opt/coremedia/cae-live-2/server-lib",
    "/opt/coremedia/cae-live-2/common-lib",
    "/opt/coremedia/cae-live-2/current/webapps/cae-live-2/WEB-INF/properties/corem",
    "/var/cache/coremedia/cae-live-2/blobstore-assets"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/cae-live-1.fact",
    "/etc/ansible/facts.d/cae-live-2.fact",
    "/opt/coremedia/cae-live-1/common-lib/coremedia-tomcat.jar",
    "/opt/coremedia/cae-live-1/current/bin/setenv.sh",
    "/opt/coremedia/cae-live-1/cae-live-1.properties",
    "/opt/coremedia/cae-live-2/common-lib/coremedia-tomcat.jar",
    "/opt/coremedia/cae-live-2/current/bin/setenv.sh",
    "/opt/coremedia/cae-live-2/cae-live-2.properties"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("coremedia").exists
    assert host.user("cae-live-1").exists
    assert host.user("cae-live-2").exists
    assert host.group("coremedia").exists
